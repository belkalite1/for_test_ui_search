FROM python:3.8
RUN mkdir /app
WORKDIR /app

COPY . /app
RUN pip install -r requirements.txt
RUN pytest --alluredir=allure_report --clean-alluredir test_search.py
CMD /bin/bash
