from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import allure


@allure.title("test_search")
def test_search():
    with allure.step("create driver"):
        # driver = webdriver.Chrome()
        driver = webdriver.Remote("http://10.10.15.18:4444/wd/hub", DesiredCapabilities.CHROME)
        driver.get('https://yandex.by/')

    with allure.step("search text"):
        input_field = driver.find_element_by_id('text')
        input_field.send_keys('Минск')
        search_button = driver.find_element_by_xpath("//button[@type='submit']")
        search_button.click()

    with allure.step("assert result"):
        header_country = driver.find_element_by_css_selector('.entity-search__header .entity-search__subtitle')
        try:
            assert "Беларуси" in header_country.text, 'text is not on the page'
        except AssertionError as err:
            raise err
        finally:
            driver.quit()
